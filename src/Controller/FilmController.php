<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class FilmController
{
    function getAllFilms() {
        $films= [
            ["title" => "Spider Man",
            "year" => "2002",
            "poster" => "http://fr.web.img5.acsta.net/r_1920_1080/medias/nmedia/00/00/00/33/spiderman.jpg",
            "synopsis" => "l'histoire d'un homme qui s'est fait piquer par une araignée..."],
            ["title" => "The Conjuring",
            "year" => "2013",
            "poster" => "https://media.altpress.com/uploads/2018/06/thenun_movie.jpg",
            "synopsis" => "l'histoire d'une bonne soeur"],

        ];
        $jsonResponse = json_encode($films);

        return new Response($jsonResponse);
    }

    function getFilm($id) {

        $films= [
            ["title" => "Spider Man",
            "year" => "2002",
            "poster" => "http://fr.web.img5.acsta.net/r_1920_1080/medias/nmedia/00/00/00/33/spiderman.jpg",
            "synopsis" => "l'histoire d'un homme qui s'est fait piquer par une araignee..."],
            ["title" => "The Conjuring",
            "year" => "2013",
            "poster" => "https://media.altpress.com/uploads/2018/06/thenun_movie.jpg",
            "synopsis" => "l'histoire d'une bonne soeur"],

            ];

        $film= [
            "id" => $id,
            "title" => $films[$id]["title"],
            "year" => $films[$id]["year"],
            "poster" => $films[$id]["poster"],
            "synopsis" => $films[$id]["synopsis"],
        ];

        $jsonResponse = json_encode($film);

        return new Response($jsonResponse);
    }

    function deleteFilm($id){
        $jsonResponse = json_encode([]);
        return new Response($jsonResponse);
    }
//methode POST
    function createFilm(Request $request){
        $body = $request->getContent();
        $body = json_decode($body);
        $responseJson = json_decode([]);
        return new Response($responseJson, 201);
    }

    //methode PUT
    function modificationFilm(Request $request, $id){
        $body = $request->getContent();
        $body = json_decode($body);

//--------Récupération de la save puis la modifier par la suite


        $responseJson = json_decode([]);
        return new Response($responseJson, 200);
    }


}