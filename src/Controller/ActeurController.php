<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ActeurController
{
    function getAllActeurs() {
        $acteurs= [
        ["prenom" => "John",
            "nom" => "Doe"],
            ["prenom" => "Mary-Jane",
            "nom" => "Watson"],
        ];
        $jsonResponse = json_encode($acteurs);

        return new Response($jsonResponse);
    }

    function getActeur($id) {
        $acteurs= [
            ["prenom" => "John",
            "nom" => "Doe"],
            ["prenom" => "Mary-Jane",
            "nom" => "Watson"],
        ];
        $acteur= [
            "id" => $id,
            "prenom" => $acteurs[$id]["prenom"],
            "nom"=>$acteurs[$id]["nom"]
        ];

        $jsonResponse = json_encode($acteur);

        return new Response($jsonResponse);
    }

    function deleteActeur($id){
        $jsonResponse = json_encode([]);
        return new Response($jsonResponse);
    }
//methode POST
    function createActeur(Request $request){
        $body = $request->getContent();
        $body = json_decode($body);
        $responseJson = json_decode([]);
        return new Response($responseJson, 201);
    }

  //methode PUT
    function modificationActeur(Request $request, $id){
    $body = $request->getContent();
    $body = json_decode($body);

//--------Récupération de la save puis la modifier par la suite


        $responseJson = json_decode([]);
        return new Response($responseJson, 200);
}



}