<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategorieController
{
    function getAllCategories() {
        $categories= [
            ["nom" => "film d'action"],
            ["nom" => "film d'horreur"],

        ];
        $jsonResponse = json_encode($categories);

        return new Response($jsonResponse);
    }

    function getCategorie($id) {
        $categories= [
            ["nom" => "film d'action"],
            ["nom" => "film d'horreur"],

        ];

        $categorie= [
            "id" => $id,
            "nom" => $categories[$id]["nom"]
        ];

        $jsonResponse = json_encode($categorie);

        return new Response($jsonResponse);
    }

    function deleteCategorie($id){
        $jsonResponse = json_encode([]);
        return new Response($jsonResponse);
    }
//methode POST
    function createCategorie(Request $request){
        $body = $request->getContent();
        $body = json_decode($body);
        $responseJson = json_decode([]);
        return new Response($responseJson, 201);
    }

    //methode PUT
    function modificationCategorie(Request $request, $id){
        $body = $request->getContent();
        $body = json_decode($body);

//--------Récupération de la save puis la modifier par la suite


        $responseJson = json_decode([]);
        return new Response($responseJson, 200);
    }

}